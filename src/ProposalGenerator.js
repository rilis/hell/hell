"use strict";
var algorithms = require("./algorithms.js");
var log = require("./logger.js");
var chalk = require("chalk");

var explainPrefix = chalk.blue.bold("[EXPLAIN] ");

function configToArray(config) {
    var result = [];
    for (var repoUrl in config) {
        result.push({
            source: repoUrl,
            version: config[repoUrl].version,
            required: config[repoUrl].required,
            targets: config[repoUrl].targets,
            requiredBy: config[repoUrl].requiredBy
        });
    }
    return result;
}

function getNumberOfPermutations(config) {
    var numPerms = 1;
    for (var i = 0; i < config.length; ++i) {
        numPerms *= config[i].version.length;
    }
    return numPerms;
}

function getDivisors(config) {
    var divisors = [];
    for (var i = config.length - 1; i >= 0; --i) {
        divisors[i] = divisors[i + 1] ? divisors[i + 1] * config[i + 1].version.length : 1;
    }
    return divisors;
}

var tagValidationRegex = /\w*=\w*/i;

function haveInvalidTargets(explainPadding, solution, getRepoInfo) {
    function getConfig(source, version) {
        if (source === "root") {
            return getRepoInfo(source).config;
        }
        return getRepoInfo(source).reduce(function(c, v) {
            if (v.version === version) {
                return v.config;
            } else {
                return c;
            }
        }, {});
    }

    function targetsAreConflictiong(targets) {
        return algorithms.getUniques(targets.map(function(target) {
            return target.substring(0, target.indexOf('='));
        })).length !== targets.length;
    }

    for (var source in solution) {
        if (targetsAreConflictiong(solution[source].targets)) {
            // check if targets are not conflicting for given source and version
            log("explain")(explainPrefix + explainPadding + chalk.yellow.bold("  This wont't work.") + " Targets: %j for %s are conflicting. These are required by: \n%s", solution[source].targets, source, JSON.stringify(solution[source].requiredBy, null, 2));
            log("debug")("haveInvalidTargets: conflicting");
            return true;
        }

        for (var targetNumber in solution[source].targets) {
            if (!solution[source].targets[targetNumber].match(tagValidationRegex)) {
                // check if target is in form A=B
                log("explain")(explainPrefix + explainPadding + chalk.yellow.bold("  This wont't work.") + " Target: '%s' for %s is not in X=Y form.", solution[source].targets[targetNumber], source);
                log("debug")("haveInvalidTargets: not match to tagValidationRegex");
                return true;
            } else if (!(solution[source].targets[targetNumber] in getConfig(source, solution[source].version))) {
                // check if targets exists for all sources in given versions in repo db
                log("explain")(explainPrefix + explainPadding + chalk.yellow.bold("  This wont't work.") + " Target: '%s' do not exist for %s@%s.", solution[source].targets[targetNumber], source, solution[source].version);
                log("debug")("haveInvalidTargets: target not exist (%j %j vs %j)",
                    source,
                    solution[source].targets[targetNumber],
                    getConfig(source, solution[source].version));
                return true;
            }
        }
    }
    return false;
}

function mergePartialSolutions(explainPadding, oldSolution, newSolution, getRepoInfo) {

    var merged = JSON.parse(JSON.stringify(oldSolution));
    for (var repo in newSolution) {
        if (newSolution[repo].source in merged &&
            merged[newSolution[repo].source].version !== newSolution[repo].version) {
            log("explain")(explainPrefix + explainPadding + chalk.yellow.bold("  This wont't work.") + " %s can't at the same time have version %s and %s!",
                newSolution[repo].source, merged[newSolution[repo].source].version,
                newSolution[repo].version);
            return null;
        }
        if (merged[newSolution[repo].source] === undefined) {
            merged[newSolution[repo].source] = {
                targets: [],
                requiredBy: []
            };
        }
        merged[newSolution[repo].source].version = newSolution[repo].version;
        merged[newSolution[repo].source].targets = algorithms
            .getUniques(merged[newSolution[repo].source].targets.concat(newSolution[repo].targets));
        merged[newSolution[repo].source].requiredBy = algorithms
            .getUniques(merged[newSolution[repo].source].requiredBy.concat(newSolution[repo].requiredBy));
    }
    if (haveInvalidTargets(explainPadding, merged, getRepoInfo)) {
        log("debug")("Invalid targets detected for: %j", merged);
        return null;
    } else if (algorithms.haveCycle(merged)) {
        log("explain")(explainPrefix + explainPadding + chalk.yellow.bold("  This wont't work.") + " Circular dependency has beed detected!");
        log("debug")("Cycle detected for: %j", merged);
        return null;
    } else {
        return merged;
    }
}

function ProposalGenerator(getRepoInfo, config, baseSolution, explainDepth) {

    this._getRepoInfo = getRepoInfo;
    this._config = configToArray(config);
    this._baseSolution = baseSolution;
    this._proposalCounter = 0;
    this._numberOfPermutations = getNumberOfPermutations(this._config);
    this._divisors = getDivisors(this._config);
    if (explainDepth !== undefined) {
        this._explainPadding = " ".repeat(explainDepth * 2);
    } else {
        this._explainPadding = "";
    }
    var padding = this._explainPadding;

    log("explain")(explainPrefix + padding + chalk.green("Resolving deeper dependencies") + " (number of options: %d):", this._numberOfPermutations);

    this._config.forEach(function(c) {
        if (c.version.length === 0) {
            log("explain")(explainPrefix + padding + chalk.yellow(" " + c.source) + " do not have version accepted by dependents:");
            c.requiredBy.forEach(function(d) {
                if (d !== "root") {
                    log("explain")(explainPrefix + padding + "  * %s", d + "@" + baseSolution[d].version);
                } else {
                    log("explain")(explainPrefix + padding + "  * root");
                }
            });
        } else {
            log("explain")(explainPrefix + padding + chalk.green(" " + c.source) + " have versions %j accepted by all dependents.", c.version);
        }
    });
}

function getConfigFromRepoInfoVersion(repoInfo, interestingVersion) {
    return repoInfo.filter(function(v) {
        return v.version === interestingVersion;
    })[0].config;
}

ProposalGenerator.prototype._getPermutation = function() {
    try {
        var config = this._config;
        var divisors = this._divisors;
        var n = this._proposalCounter;
        var getRepoInfo = this._getRepoInfo;

        var configs = [];
        var partial = [];
        log("explain")(explainPrefix + this._explainPadding + " Trying option: %d", n + 1);
        for (var isource = 0; isource < config.length; ++isource) {
            var curArray = config[isource].version;
            var choosenVersion = curArray[Math.floor(n / divisors[isource]) % curArray.length];
            log("explain")(explainPrefix + this._explainPadding + chalk.green.bold("  Adding:") + " %s@%s", config[isource].source, choosenVersion);
            configs.push({
                config: getConfigFromRepoInfoVersion(getRepoInfo(config[isource].source), choosenVersion),
                required: config[isource].required,
                requiredBy: config[isource].source
            });
            partial.push({
                source: config[isource].source,
                version: choosenVersion,
                targets: config[isource].targets,
                requiredBy: config[isource].requiredBy
            });
        }
        var mergedPartial = mergePartialSolutions(this._explainPadding, this._baseSolution, partial, getRepoInfo);
        if (mergedPartial !== null) {
            return {
                configs: configs,
                partial: mergedPartial
            };
        }
    } catch (e) {
        log("error")("%s", e.message);
        log("stack")("%s", e.stack);
    }
    return null;
};

ProposalGenerator.prototype.next = function() {
    while (this._proposalCounter < this._numberOfPermutations) {
        var proposal = this._getPermutation();
        this._proposalCounter++;
        if (proposal !== null) {
            return proposal;
        }
    }
    log("explain")(explainPrefix + this._explainPadding + chalk.yellow.bold("There are no more options to check. Need to step back."));
    return null;
};

module.exports = ProposalGenerator;