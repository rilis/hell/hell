'use strict';
var git = require("./git.js");
var fs = require("fs");
var path = require("path");
var log = require("./logger.js");
var ensureDirSync = require("./helpers.js").ensureDirSync;
var existDirSync = require("./helpers.js").existDirSync;
var rmRecursiveSync = require("./helpers.js").rmRecursiveSync;
var createRepoHash = require("./algorithms.js").createRepoHash;
var chalk = require("chalk");

function readConfigFromTag(repo, tag, repoPath) {
    try {
        repo.checkout("--quiet", "--force", repo.getVersionHash(tag));
        return {
            version: tag,
            config: JSON.parse(fs.readFileSync(path.join(repoPath, ".hell.json"), "utf-8"))
        };
    } catch (e) {
        return {
            version: tag,
            config: null
        };
    }
}

function convertToResult(repo) {
    var result = [];
    for (var tag in repo) {
        result.push({
            version: tag,
            config: repo[tag]
        });
    }
    return result;
}

function RepoDb(workspace) {
    this._workspace = workspace;
    this._db = {};
    ensureDirSync(this._workspace);
}

RepoDb.prototype._createRepoPath = function(repoUrl) {
    return path.join(this._workspace, "repodb", createRepoHash(repoUrl));
};

RepoDb.prototype.getInfo = function(repoUrl) {
    if (repoUrl === "root") {
        return {
            config: this._db.root
        };
    }

    var self = this;

    if (repoUrl in self._db) {
        return convertToResult(self._db[repoUrl]);
    } else {
        self._db[repoUrl] = {};
        var repoPath = self._createRepoPath(repoUrl);
        log("info")(chalk.green("  %s"), repoUrl);

        if (existDirSync(repoPath)) {
            rmRecursiveSync(repoPath);
        }
        var repo = git(repoPath);
        ensureDirSync(repoPath);
        repo.clone(repoUrl, repoPath);

        var configs = repo.getVersions();

        configs.map(function(tag) {
            log("info")("    %s", chalk.magenta(tag));
            return readConfigFromTag(repo, tag, repoPath);
        }).filter(function(config) {
            return config.config !== null;
        }).forEach(function(config) {
            self._db[repoUrl][config.version] = config.config;
        });
        return convertToResult(self._db[repoUrl]);
    }
};

RepoDb.prototype.setRootConfig = function(config) {
    this._db.root = config;
};

module.exports = RepoDb;