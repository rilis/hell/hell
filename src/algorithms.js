"use strict";
var crypto = require("crypto");

function topoSort(dependencyGraph) {
    var grey = true;
    var green = false;

    function dfsSort(graph, currentNode, stack) {
        if (graph[currentNode].color === grey) {
            throw new Error(stack);
        } else if (graph[currentNode].color !== green) {
            graph[currentNode].color = grey;
            for (var node in graph[currentNode].requiredBy) {
                dfsSort(graph, graph[currentNode].requiredBy[node], stack);
            }
            graph[currentNode].color = green;
            stack.push(currentNode);
        }
    }

    var stack = [];
    var graph = JSON.parse(JSON.stringify(dependencyGraph));
    for (var node in graph) {
        if (graph[node].color === undefined) {
            dfsSort(graph, node, stack);
        }
    }
    return stack;
}

function haveCycle(dependencyGraph) {
    try {
        topoSort(dependencyGraph);
        return false;
    } catch (e) {
        return true;
    }
}

function getUniques(array) {
    array.sort();
    return array.reduce(function(uniques, current) {
        if (uniques.length === 0 || uniques[uniques.length - 1] !== current) {
            uniques.push(current);
        }
        return uniques;
    }, []);
}

function createRepoHash(repoUrl) {
    var sha1 = crypto.createHash('sha1');
    sha1.update(repoUrl);
    return sha1.digest('hex');
}

module.exports = {
    haveCycle: haveCycle,
    topoSort: topoSort,
    getUniques: getUniques,
    createRepoHash: createRepoHash
};