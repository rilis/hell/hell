"use strict";
var https = require('https');
var URL = require("url").URL;

function promiseSendRequest(userRequest, path) {
    return new Promise(function(resolve, reject) {
        var dataToSend = JSON.stringify(userRequest);
        var post_options = {
            host: "hell.rilis.io",
            path: path,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': dataToSend.length
            }
        };

        var post_req = https.request(post_options, function(res) {
            if (res.statusCode === 200) {
                var data = "";
                res.setEncoding('utf8');
                res.on('data', function(chunk) {
                    data += chunk;
                });
                res.on('end', function() {
                    resolve(data);
                });
            } else {
                reject("invalid error code: " + res.statusCode);
            }
            res.on('error', function(err) {
                reject(err);
            });

        });

        post_req.on('error', function(err) {
            reject(err);
        });

        post_req.write(dataToSend);
        post_req.end();
    }).then(function(data) {
        return JSON.parse(data);
    });
}

function publish(repos) {
    return promiseSendRequest(repos.filter(function(repo) {
        try {
            var url = new URL(repo);
            if ((url.host === "github.com" || url.host === "gitlab.com") && url.protocol === "https:" && url.pathname.length > 4 && url.pathname.substr(url.pathname.length - 4) === ".git") {
                return true;
            } else {
                return false;
            }
        } catch (e) {
            return false;
        }
    }), "/publish");
}

function search(userRequest) {
    return promiseSendRequest({
        request: userRequest
    }, "/search").then(function(data) {
        return data;
    }, function() {
        return [];
    });
}

module.exports = {
    search: search,
    publish: publish
};