"use strict";
var fs = require("fs");
var path = require("path");
var crypto = require("crypto");
var helpers = require("./helpers.js");

function getFileHash(filePath) {
    return crypto.createHash('sha1').update(fs.readFileSync(filePath)).digest('hex');
}

function scanDir_helper(dirPath) {
    var result = [];
    if (fs.statSync(dirPath).isDirectory()) {
        var files = fs.readdirSync(dirPath);
        if (files.length > 0) {
            for (var i = 0; i < files.length; i++) {
                var filePath = path.join(dirPath, files[i]);
                var stats = fs.statSync(filePath);
                if (stats.isFile()) {
                    result.push({
                        name: filePath,
                        type: "file",
                        hash: getFileHash(filePath)
                    });
                } else if (stats.isDirectory()) {
                    result.push({
                        name: filePath,
                        type: "dir",
                        hash: null
                    });
                    result = result.concat(scanDir_helper(filePath));
                }
            }
        }
    }
    return result;
}

function scanDir(dirPath) {
    return scanDir_helper(dirPath).filter(function(entry) {
        var cmp = path.join(dirPath, ".git");
        return !entry.name.startsWith(cmp + path.sep) && entry.name !== cmp;
    }).reduce(function(acc, entry) {
        acc[entry.name.substr(dirPath.length)] = {
            type: entry.type,
            hash: entry.hash
        };
        return acc;
    }, {});
}

function forceRemove(somePath) {
    try {
        var stats = fs.statSync(somePath);
        if (stats.isFile()) {
            fs.unlinkSync(somePath);
        } else if (stats.isDirectory()) {
            helpers.rmRecursiveSync(somePath);
        }
    } catch (e) {}
}

function forceCopyFile(source, destination) {
    helpers.ensureDirSync(path.dirname(destination));
    fs.writeFileSync(destination, fs.readFileSync(source));
}

function forceCopyDir(source, destination) {
    helpers.ensureDirSync(destination);
    var files = fs.readdirSync(source);
    for (var i = 0; i < files.length; i++) {
        forceCopy(path.join(source, files[i]), path.join(destination, files[i]));
    }
}

function forceCopy(source, destination) {
    var stats = fs.statSync(source);
    if (stats.isFile()) {
        forceCopyFile(source, destination);
    } else if (stats.isDirectory()) {
        forceCopyDir(source, destination);
    }
}

function updateDirectory(sourceDir, destinationDir) {
    helpers.ensureDirSync(destinationDir);
    var sourceDirContents = scanDir(sourceDir);
    var destinationDirContents = scanDir(destinationDir);

    for (var i in destinationDirContents) {
        if (destinationDirContents.hasOwnProperty(i)) {
            if (!sourceDirContents.hasOwnProperty(i) || // remove stuff in dest which not exist in sourceDir
                sourceDirContents[i].hash !== destinationDirContents[i].hash || // remove stuff in dest which have different type or hash than in sourceDir
                sourceDirContents[i].type !== destinationDirContents[i].type) {

                forceRemove(path.join(destinationDir, i));
                delete destinationDirContents[i];
            }
        }
    }

    // copy stuff from sourceDir which are missing in destinationDir
    for (var j in sourceDirContents) {
        if (sourceDirContents.hasOwnProperty(j) && !destinationDirContents.hasOwnProperty(j)) {
            forceCopy(path.join(sourceDir, j), path.join(destinationDir, j));
        }
    }
}

module.exports = updateDirectory;