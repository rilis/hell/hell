'use strict';
var Resolver = require("../src/Resolver.js");
var expect = require("chai").expect;

var config1 = {
    depA: [{
            source: "repo1",
            version: "<3.2.1",
            required: [
                "depA"
            ]
        },
        {
            source: "repo2",
            version: "*",
            required: [
                "depB",
                "depA"
            ]
        }
    ],
    depC: [{
            source: "repo3",
            version: "*",
            required: [
                "depB",
                "depC"
            ]
        },
        {
            source: "repo1",
            version: ">1.2.3",
            required: [
                "depF"
            ]
        }
    ],
    depD: [{
        source: "repo4",
        version: "1.2.3",
        required: [
            "depG"
        ]
    }]
};

var config2 = {
    depH: [{
            source: "repo1",
            version: "*",
            required: [
                "depE"
            ]
        },
        {
            source: "repo2",
            version: ">1.2.3",
            required: [
                "depE"
            ]
        }
    ],
    depI: [{
        source: "repo5",
        version: "7.7.7",
        required: []
    }]
};

describe("Resolver", function() {
    describe("mergeConfigs", function() {
        it("merge single empty config", function() {
            expect({}).to.deep.equal(Resolver._mergeConfigs([{
                config: {},
                required: []
            }]));
        });

        it("marge two empty configs", function() {
            expect({}).to.deep.equal(Resolver._mergeConfigs([{
                config: {},
                required: []
            }, {
                config: {},
                required: []
            }]));
        });

        it("merge single internaly complex config", function() {
            expect({
                repo1: {
                    version: ["<3.2.1", ">1.2.3"],
                    required: ["depA", "depF"],
                    targets: ["depA", "depF"],
                    requiredBy: ["root"]
                },
                repo2: {
                    version: ["*"],
                    required: ["depB", "depA"],
                    targets: ["depB", "depA"],
                    requiredBy: ["root"]
                },
                repo3: {
                    version: ["*"],
                    required: ["depB", "depC"],
                    targets: ["depB", "depC"],
                    requiredBy: ["root"]
                },
                repo4: {
                    version: ["1.2.3"],
                    required: ["depG"],
                    targets: ["depG"],
                    requiredBy: ["root"]
                }
            }).to.deep.equal(Resolver._mergeConfigs([{
                config: config1,
                required: ["depA", "depC", "depD"],
                requiredBy: ["root"]
            }]));
        });

        it("merge two internally complex configs which overlap", function() {
            expect({
                repo1: {
                    version: ["*", "<3.2.1", ">1.2.3"],
                    required: ["depA", "depE", "depF"],
                    targets: ["depA", "depE", "depF"],
                    requiredBy: ["A", "root"]
                },
                repo2: {
                    version: ["*", ">1.2.3"],
                    required: ["depA", "depB", "depE"],
                    targets: ["depA", "depB", "depE"],
                    requiredBy: ["A", "root"]
                },
                repo3: {
                    version: ["*"],
                    required: ["depB", "depC"],
                    targets: ["depB", "depC"],
                    requiredBy: ["A"]
                },
                repo4: {
                    version: ["1.2.3"],
                    required: ["depG"],
                    targets: ["depG"],
                    requiredBy: ["A"]
                },
                repo5: {
                    version: ["7.7.7"],
                    required: [],
                    targets: [],
                    requiredBy: ["root"]
                }
            }).to.deep.equal(Resolver._mergeConfigs([{
                config: config1,
                required: ["depA", "depC", "depD"],
                requiredBy: ["A"]
            }, {
                config: config2,
                required: ["depH", "depI"],
                requiredBy: ["root"]
            }]));
        });
    });
});