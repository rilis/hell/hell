'use strict';
var Resolver = require("../src/Resolver.js");
var expect = require("chai").expect;

describe("Resolver", function() {
    describe("resolve", function() {

        var resolverDefaultOptions = {
            deepSearch: false,
            prereleases: false
        };

        function createGetInfoFromStub(stub, rootConfig) {
            return function(repoUrl) {
                if (repoUrl !== "root") {
                    expect(stub).to.have.property(repoUrl);
                    return stub[repoUrl];
                } else {
                    return rootConfig;
                }
            };
        }

        it("when no dependencies then single empty solution", function() {
            var getRepoInfo = createGetInfoFromStub({});
            var resolver = new Resolver(getRepoInfo, resolverDefaultOptions);
            var expected = {
                root: {
                    requiredBy: [],
                    targets: []
                }
            };
            var searched = {
                config: {},
                required: []
            };
            expect(expected).to.deep.equal(resolver.resolve(searched));
        });

        it("when single dependency and only one version known, which match requirements, then single solution", function() {
            var searched = {
                config: {
                    "depA=ON": [{
                        source: "repo1",
                        version: "1.2.3",
                        required: ["depB=ON"]
                    }]
                },
                required: ["depA=ON"]
            };
            var getRepoInfo = createGetInfoFromStub({
                repo1: [{
                    version: "1.2.3",
                    config: {
                        "depB=ON": []
                    }
                }]
            }, {
                config: searched.config
            });
            var resolver = new Resolver(getRepoInfo, resolverDefaultOptions);
            var expected = {
                repo1: {
                    version: "1.2.3",
                    targets: ["depB=ON"],
                    requiredBy: ["root"]
                },
                root: {
                    requiredBy: [],
                    targets: ["depA=ON"]
                }
            };
            expect(expected).to.deep.equal(resolver.resolve(searched));
        });

        it("when single dependency and two versions known, which match requirements, then first found will be used", function() {
            var searched = {
                config: {
                    "depA=ON": [{
                        source: "repo1",
                        version: ">1.2.0",
                        required: ["depB=ON"]
                    }]
                },
                required: ["depA=ON"]
            };

            var getRepoInfo = createGetInfoFromStub({
                repo1: [{
                    version: "1.2.1",
                    config: {
                        "depB=ON": []
                    }
                }, {
                    version: "1.2.2",
                    config: {
                        "depB=ON": []
                    }
                }]
            }, {
                config: searched.config
            });

            var resolver = new Resolver(getRepoInfo, resolverDefaultOptions);
            var expected = {
                repo1: {
                    version: "1.2.1",
                    targets: ["depB=ON"],
                    requiredBy: ["root"]
                },
                root: {
                    requiredBy: [],
                    targets: ["depA=ON"]
                }
            };

            expect(expected).to.deep.equal(resolver.resolve(searched));
        });

        it("when trivial circular then not resolved", function() {
            var searched = {
                config: {
                    "depA=ON": [{
                        source: "repo1",
                        version: "1.0.0",
                        required: ["depA=ON"]
                    }]
                },
                required: ["depA=ON"]
            };

            var getRepoInfo = createGetInfoFromStub({
                repo1: [{
                    version: "1.0.0",
                    config: {
                        "depA=ON": [{
                            source: "repo2",
                            version: "2.0.0",
                            required: [
                                "depA=ON"
                            ]
                        }]
                    }
                }],
                repo2: [{
                    version: "2.0.0",
                    config: {
                        "depA=ON": [{
                            source: "repo1",
                            version: "1.0.0",
                            required: [
                                "depA=ON"
                            ]
                        }]
                    }
                }]
            }, {
                config: searched.config
            });

            var resolver = new Resolver(getRepoInfo, resolverDefaultOptions);
            var expected = null;

            expect(expected).to.deep.equal(resolver.resolve(searched));
        });

        it("when dependency not exist in sub-dependency then failure", function() {
            var searched = {
                config: {
                    "depA=ON": [{
                        source: "repo1",
                        version: "1.2.3",
                        required: ["not existing"]
                    }]
                },
                required: ["depA=ON"]
            };

            var getRepoInfo = createGetInfoFromStub({
                repo1: [{
                    version: "1.2.3",
                    config: {
                        /*here is no "not existing" target*/
                    }
                }]
            }, {
                config: searched.config
            });
            var resolver = new Resolver(getRepoInfo, resolverDefaultOptions);
            var expected = null;
            expect(expected).to.deep.equal(resolver.resolve(searched));
        });

        it("when 4 repo diamond dependency then resolved", function() {
            var searched = {
                config: {
                    "depA=ON": [{
                        source: "repo1",
                        version: "1.0.0",
                        required: ["dep1=ON"]
                    }]
                },
                required: ["depA=ON"]
            };
            var getRepoInfo = createGetInfoFromStub({
                repo1: [{
                    version: "1.0.0",
                    config: {
                        "dep1=ON": [{
                            source: "repo2",
                            version: "2.0.0",
                            required: ["dep2=ON"]
                        }, {
                            source: "repo3",
                            version: "3.0.0",
                            required: ["dep3=ON"]
                        }]
                    }
                }],
                repo2: [{
                    version: "2.0.0",
                    config: {
                        "dep2=ON": [{
                            source: "repo4",
                            version: "4.0.0",
                            required: ["dep4=ON"]
                        }]
                    }
                }],
                repo3: [{
                    version: "3.0.0",
                    config: {
                        "dep3=ON": [{
                            source: "repo4",
                            version: "4.0.0",
                            required: ["dep4=ON"]
                        }]
                    }
                }],
                repo4: [{
                    version: "4.0.0",
                    config: {
                        "dep4=ON": []
                    }
                }]
            }, {
                config: searched.config
            });
            var resolver = new Resolver(getRepoInfo, resolverDefaultOptions);
            var expected = {
                repo1: {
                    version: "1.0.0",
                    targets: ["dep1=ON"],
                    requiredBy: ["root"]
                },
                repo2: {
                    version: "2.0.0",
                    targets: ["dep2=ON"],
                    requiredBy: ["repo1"]
                },
                repo3: {
                    version: "3.0.0",
                    targets: ["dep3=ON"],
                    requiredBy: ["repo1"]
                },
                repo4: {
                    version: "4.0.0",
                    targets: ["dep4=ON"],
                    requiredBy: ["repo2", "repo3"]
                },
                root: {
                    requiredBy: [],
                    targets: ["depA=ON"]
                }
            };

            expect(expected).to.deep.equal(resolver.resolve(searched));
        });

        it("when single dependency and targets conflicting then failure", function() {
            var searched = {
                config: {
                    "depA=ON": [{
                        source: "repo1",
                        version: "1.2.3",
                        required: ["depC=ON"]
                    }],
                    "depB=ON": [{
                        source: "repo1",
                        version: "1.2.3",
                        required: ["depC=OFF"]
                    }],
                },
                required: ["depA=ON", "depB=ON"]
            };
            var getRepoInfo = createGetInfoFromStub({
                repo1: [{
                    version: "1.2.3",
                    config: {
                        "depC=ON": [],
                        "depC=OFF": []
                    }
                }]
            }, {
                config: searched.config
            });
            var resolver = new Resolver(getRepoInfo, resolverDefaultOptions);
            var expected = null;
            expect(expected).to.deep.equal(resolver.resolve(searched));
        });

        it("default target('=') can be used", function() {
            var searched = {
                config: {
                    "=": [{
                        source: "repo1",
                        version: "1.2.3",
                        required: ["="]
                    }]
                },
                required: ["="]
            };
            var getRepoInfo = createGetInfoFromStub({
                repo1: [{
                    version: "1.2.3",
                    config: {
                        "=": []
                    }
                }]
            }, {
                config: searched.config
            });
            var resolver = new Resolver(getRepoInfo, resolverDefaultOptions);
            var expected = {
                repo1: {
                    version: "1.2.3",
                    targets: ["="],
                    requiredBy: ["root"]
                },
                root: {
                    requiredBy: [],
                    targets: ["="]
                }
            };
            expect(expected).to.deep.equal(resolver.resolve(searched));
        });
    });
});